package com.ramu.actuator;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.stereotype.Component;
@Endpoint(id = "custom")
@Component
public class MainController {
	
	@ReadOperation
	public List<Integer> getListOfName() {
		
		List<Integer>  names=new ArrayList<>();
		names.add(1);
		names.add(2);
		return names;
	}
}
