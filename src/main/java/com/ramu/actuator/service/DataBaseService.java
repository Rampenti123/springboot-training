package com.ramu.actuator.service;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class DataBaseService implements HealthIndicator{

	private final String SERVICE_NAME="database_service";
	@Override
	public Health health() {
		if(isDataBaseGood())
		return Health.up().withDetail(SERVICE_NAME,"Database service is Up").build();
		return Health.down().withDetail(SERVICE_NAME,"Database service is down").build();
	}
	public boolean isDataBaseGood() {
//		value
		return true;
	}

	
}
